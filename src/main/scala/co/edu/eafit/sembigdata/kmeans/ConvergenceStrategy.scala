/**
 * Copyright 2017 Semillero de Investigación en Big Data - Universidad EAFIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.edu.eafit.sembigdata.spark.kmeans

import scala.collection.Seq
import scala.reflect.ClassTag

/** A class to represent a convergence strategy for the KMeans algorithm
  *
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.2.10 - 21/05/2017
  * @since 1.0.0 - 13/10/2016
  */
abstract class ConvergenceStrategy {
  /** The convergence logic goes here
    *
    * @param oldMeans The means computed in the previous step
    * @param newMeans The means calculated in the actual step
    * @tparam P The type of the points, must be a subtype of Point
    * @tparam PF The type of the pointFunctions, must be a subtype PointFunctions[P]
    *
    * @return true if the algorithm already converged else false
    *
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.PointFunctions PointFunctions]]
    */
  def converged[P <: Point, PF <: PointFunctions[P]](oldMeans: Seq[P], newMeans: Seq[P])
    (implicit pct: ClassTag[P], pointToFunctions: P => PF): Boolean
}

/** Convergence Strategy based on the number of steps that the algorithm must execute
  *
  * @constructor Create a new Convergence Strategy with the number of steps to perform
  * @param n The amount of steps that the algorithm will perform,
  * this value must be greater than zero
  *
  * @note throws '''IllegalArgumentException''' If n <= 0
  *
  * @see [[co.edu.eafit.sembigdata.spark.kmeans.ConvergenceStrategy ConvergenceStrategy]]
  *
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.2.10 - 21/05/2017
  * @since 1.0.0 - 13/10/2016
  */
case class ConvergedAfterNSteps(private val n: Int) extends ConvergenceStrategy {
  require(n > 0)
  private var steps = 0;

  /** If the actual step is greater or equal to n the algorithm converged
    *
    * @param oldMeans The means computed in the previous step
    * @param newMeans The means calculated in the actual step
    * @tparam P The type of the points, must be a subtype of Point
    * @tparam PF The type of the pointFunctions, must be a subtype PointFunctions[P]
    *
    * @return true if the algorithm already converged else false
    *
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.PointFunctions PointFunctions]]
    */
  override def converged[P <: Point, PF <: PointFunctions[P]](oldMeans: Seq[P], newMeans: Seq[P])
    (implicit pct: ClassTag[P], pointToFunctions: P => PF): Boolean = {
    steps += 1
    steps >= n
  }
}

/** Convergence Strategy based on the change undergone by the means.
  *
  * @constructor Create a new Convergence Strategy with the desired value for eta
  * @param eta The value for the maximum difference between the old and new means
  *
  * @note throws an '''IllegalArgumentException''' If eta isn't in range (0, 1) exclusive
  *
  * @see [[co.edu.eafit.sembigdata.spark.kmeans.ConvergenceStrategy ConvergenceStrategy]]
  *
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.2.10 - 21/05/2017
  * @since 1.0.0 - 13/10/2016
  */
case class ConvergedAfterMeansAreStill(private val eta: Double) extends ConvergenceStrategy {
  require(eta > 0 && eta < 1)

  /** If the difference between the oldMeans and the newMeans is less of equal
    * to eta the algorithm converged
    *
    * @param oldMeans The means computed in the previous step
    * @param newMeans The means calculated in the actual step
    * @tparam P The type of the points, must be a subtype of Point
    * @tparam PF The type of the pointFunctions, must be a subtype PointFunctions[P]
    *
    * @return true if the algorithm already converged else false
    *
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.PointFunctions PointFunctions]]
    */
  override def converged[P <: Point, PF <: PointFunctions[P]](oldMeans: Seq[P], newMeans: Seq[P])
    (implicit pct: ClassTag[P], pointToFunctions: P => PF): Boolean = {
    (oldMeans zip newMeans) forall { case(m1, m2) => m1.distance(m2) <= eta }
  }
}
