/**
 * Copyright 2017 Semillero de Investigación en Big Data - Universidad EAFIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.edu.eafit.sembigdata.spark.kmeans

import scala.Serializable

/** A class that represents a point in a N-Dimensional space
  *
  * @constructor Create a new point with n values
  * @param values A product with the n values for this point
  *
  * @see [[scala.Product Product]]
  * @see [[scala.Serializable Serializable]]
  *
  * @note It's mandatory that any subclass of Point satisfy the following condition:
  * "Given two points p1 and p2 if they are equals then their hash codes are the same".
  * {{{ if (p1 == p2) then p1.hashCode == p2.hashCode }}}
  * The implementation of this methods in this class accomplish that condition,
  * but developers using this API could override these implementations for better ones.
  *
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.2.10 - 21/05/2017
  * @since 1.0.0 - 13/10/2016
  */
@SerialVersionUID(121020161L)
abstract class Point(val values: Product) extends Serializable {
  /** Returns a string representation of this object
    *
    * Subclasses could override this method to give a better representation
    */
  override def toString(): String = "point: " + values.toString

  /** Compares this object with other to check if they are the same or not
    *
    * Subclasses could override this method for better comparing
    * @param other The object to compare
    * @return true if other is a Point and have the same values as this, otherwise false
    *
    * @example
    * {{{
    * new Point((10, 20)) == new Point((10, 20)) //true
    * new Point((10, 20)) == new Point((20, 10)) //false
    * }}}
    *
    * @note This method is implemented as follows
    * {{{
    * other match {
    *    case that: Point => (this.values canEqual that.values) && (this.values equals that.values)
    *    case _           => false
    * }
    * }}}
    *
    * @see [[scala.Product Product]]
    */
  override def equals(other: Any): Boolean = other match {
    case that: Point => (this.values canEqual that.values) && (this.values equals that.values)
    case _           => false
  }

  /** Returns the hash code of this object
    *
    * Subclasses could override this method for better hashing generation
    *
    * @note this method is implemented as follows
    * {{{ this.values.productIterator.foldLeft(0) { (acc: Int, x: Any) => acc + x.hashCode } }}}
    *
    * @see [[scala.Product Product]]
    */
  override def hashCode(): Int = {
    this.values.productIterator.foldLeft(0) { (acc: Int, x: Any) => acc + x.hashCode }
  }
}

/** An util class for adding methods to Point
  *
  * @constructor Create a new PointFunctions wrapper for a point
  * @param point The point to wrap
  * @tparam P the type of the Point
  *
  * @note It's mandatory for developers to implement a subclass of
  * PointFunctions for their implementations of Point.
  *
  * @note the implementation of the method '+' and '/' must satisfy the following condition.
  * "Given n points p1, ... , pn then x = (p1 + ... + pn) / n  must be the point
  * in the center of the distribution"
  *
  * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
  *
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.2.10 - 21/05/2017
  * @since 1.0.0 - 13/10/2016
  */
abstract class PointFunctions[P <: Point](val point: P) {
  /** Returns the distance between two points
    * @param that The point to compute the distance to this point
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    */
  def distance(that: P): Double

  /** Computes the aggregation of two points
    * @param that The point to aggregate with this
    * @return The aggregated point
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    */
  def +(that: P): P

  /** Computes the n level disaggregation of a point
    * @param n the level of disaggregation
    * @return the disaggregated point
    *
    * @note if this point was created as the aggregation of n points,
    * then this method will return the center point of that n points
    *
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.PointFunctions.+ +]]
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    */
  def /(n: Int): P
}
