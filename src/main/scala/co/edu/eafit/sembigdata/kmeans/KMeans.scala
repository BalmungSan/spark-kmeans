/**
 * Copyright 2017 Semillero de Investigación en Big Data - Universidad EAFIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.edu.eafit.sembigdata.spark.kmeans

import scala.Serializable
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import scala.annotation.tailrec
import scala.collection.Iterable
import scala.collection.Seq
import scala.reflect.ClassTag

/** A class to represent the KMeans algorithm for a group of points
  *
  * @constructor Create a new KMeans with a group of points
  * @param points The group of points for this kmeans algorithm
  * @param castFunction a cast function from an instance of a subclass of
  * [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]] to a subclass of
  * [[co.edu.eafit.sembigdata.spark.kmeans.PointFunctions PointFunctions]]
  * @param cs The Convergence Strategy to use for the algorithm
  * @tparam P The type of the points, must be a subclass of Point
  * @tparam PF The type of the pointFunctions that points will be casted,
  * must be a subclass of PointFunctions[P]
  *
  * @note This class caches the points RDD in order to increase performance,
  *
  * @see [[org.apache.spark.rdd.RDD RDD]]
  * @see [[scala.Serializable Serializable]]
  * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
  * @see [[co.edu.eafit.sembigdata.spark.kmeans.PointFunctions PointFunctions]]
  * @see [[co.edu.eafit.sembigdata.spark.kmeans.ConvergenceStrategy ConvergenceStrategy]]
  *
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.2.10 - 21/05/2017
  * @since 1.0.0 - 13/10/2016
  */
@SerialVersionUID(121020162L)
class KMeans[P <: Point, PF <: PointFunctions[P]](points: RDD[P], castFunction: P => PF, cs: ConvergenceStrategy)
  (implicit pct: ClassTag[P], pfct: ClassTag[PF]) extends Serializable {
  //make the Point to PointFunctions cast function implicit
  implicit def cast = castFunction
  //cache points
  points.setName("points")
  points.cache()

  /** Computes the KMeans algorithm for the initial points using an initial group of means
    *
    * This method returns and RDD where each element is a cluster of the initial points.
    * Each cluster has the following structure (mean, (count, points))
    - '''mean''': The cluster's central point
    - '''count''': The amount of points in this cluster
    - '''points''': A [[scala.collection.Seq Seq]] containing all points in the cluster
    *
    * @param means The initial means to start the algorithm
    * @return A [[org.apache.spark.rdd.PairRDDFunctions PairRDD]]
    * where the keys are the computed means and the values are the points
    *
    * @note this method caches the final results in memory
    * @note If you do not have an idea of what could be the initial means you should use
    * [[co.edu.eafit.sembigdata.spark.kmeans.KMeans.kmeans(k:Int)* kmeans(k: Int)]]
    * method which will generate k random initial means
    *
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    * @see [[org.apache.spark.rdd.RDD RDD]]
    */
  @tailrec
  final def kmeans(means: Seq[P]): RDD[(P, (Int, Iterable[P]))] = {
    val classified = classify(means)
    //cache classified
    classified.setName("classified")
    classified.cache()
    val newMeans = update(classified)
    if (!cs.converged(means, newMeans)) {
      classified.unpersist(false) //release this rdd in a non-blocking way
      kmeans(newMeans)
    } else {
      val results = classified.groupByKey() mapValues { data =>
        val (points, sums) = data.unzip
        (sums.sum, points)
      }
      //saves the results in cache
      results.setName("results")
      results.cache()
      results
    }
  }

  /**
    * Compute the KMeans algorithm for points using a random generate group of k means
    *
    * This method returns and RDD of size k,
    * where each element is a cluster of the initial points.
    * Each cluster has the following structure (mean, (count, points))
    - '''mean''': The cluster's central point
    - '''count''': The amount of points in this cluster
    - '''points''': A [[scala.collection.Seq Seq]] containing all points in the cluster
    *
    * @param k The amount of random means to generate
    * @return A [[org.apache.spark.rdd.PairRDDFunctions PairRDD]]
    * where the keys are the computed means and the values are the points
    *
    * @note this method caches the final results in memory
    * @note If you have an idea of which will be the means computed by the algorithm you should use the
    * [[co.edu.eafit.sembigdata.spark.kmeans.KMeans.kmeans(means:Seq[P])* kmeans(means: Seq[P])]]
    * method which will give you better results.
    *
    * @see [[co.edu.eafit.sembigdata.spark.kmeans.Point Point]]
    * @see [[org.apache.spark.rdd.RDD RDD]]
    */
  final def kmeans(k: Int): RDD[(P, (Int, Iterable[P]))] = {
    val firstMeans = initMeans(k)
    kmeans(firstMeans)
  }

  /**
    * Take random k points from the initial set of points
    */
  private def initMeans(k: Int): Seq[P] = {
    val sc = points.sparkContext
    points.takeSample(false, k)
  }

  /**
    * Find and return the closest mean to a point
    */
  private def findClosest(point: P, means: Seq[P]): P = {
    means minBy { mean => point.distance(mean) }
  }

  /**
    * Group all points by their closest mean
    */
  private def classify(means: Seq[P]): RDD[(P, (P, Int))] = {
    points map { point => (findClosest(point, means), (point, 1)) }
  }

  /**
    * Update the means
    * Generating a new means as the center point of a group of points
    */
  private def update(classified: RDD[(P,(P, Int))]): Seq[P] = {
    val newMeans = classified reduceByKey {
      case((p1, c1), (p2, c2)) => (p1 + p2, c1 + c2)
    } map {
      case(m, (point, count)) => point / count
    }
    newMeans.collect()
  }
}
