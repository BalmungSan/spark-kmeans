/**
 * Copyright 2017 Semillero de Investigación en Big Data - Universidad EAFIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

name         := "Spark-KMeans"
version      := "1.2.10"
organization := "co.edu.eafit.sembigdata"

scalaVersion := "2.11.8"
scalacOptions += "-feature"
scalacOptions in (doc) ++= Seq("-groups", "-implicits")
scalacOptions in (Compile, doc) ++= Seq("-doc-title", "Spark-KMeans")

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.0.1"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.0.1"
