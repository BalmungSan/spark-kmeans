# spark-kmeans
Simple KMeans API for Spark

## Author
Luis Miguel Mejía Suárez (BalmungSan)

Universidad EAFIT - 2017 (Semillero de Investigación en Big Data)

## Version
1.2.10 (21/05/2017)

## API
[API Documentation](http://balmungsan.gitlab.io/spark-kmeans/#co.edu.eafit.sembigdata.spark.kmeans.package)

## Dependencies
- **Java:** The [Java 8](http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) Development Kit (JDK)
- **Scala:** The [Scala Programming Language](http://scala-lang.org/) _Version 2.11.8_
- **SBT:** The [Scala Build Tool](http://www.scala-sbt.org/) _Version >= 0.13.15_
- **Spark:** [Apache Spark](http://spark.apache.org/) _Version >= 2.0.1_

## How to use
In order to use this api you have to install it in your spark cluster, link it to your project and implement the abstract classes.

### Install
The first step is to compile and install the library in your local repository

    $ sbt clean package publishLocal

Then copy the generated jar to a location inside the spark class path

    $ cp target/scala-2.11/spark-kmeans_2.11-1.2.10.jar /path/to/spark/libs

### Link
To link this api to your project add the following line to your _build.sbt_ file  
`libraryDependencies += "co.edu.eafit.sembigdata"  %% "spark-kmeans" % "1.2.10"`  
And import the api classes in your project  
`import co.edu.eafit.sembigdata.spark.kmeans._`

### Implement
Finally you had to provide your own implementations of the **Point** and **PointFunctions** _abstract classes_

**Point:**  
The Point class is a representation of your data in a n-dimensional space.  
This class exposes the methods: _toString_, _equals_ and _hashCode_  
The abstract class provides a default implementation for this methods,
but you can override them for a more concise implementation for your expecific _values_.  
For a more detailed explanation of each method and the conditions they impose,
please refer to the [api](http://balmungsan.gitlab.io/spark-kmeans/#co.edu.eafit.sembigdata.spark.kmeans.Point)

```scala
//Implementation for a two dimensional point (X, Y)
class xyPoint(override val values: (Double, Double)) extends Point(values) {
  override def toString(): String = "Point(" + this.values._1 + ", " + this.values._2 + ")"
}
```

**PointFunctions:**  
The PointFunctions class is a wrapper for adding function to the Point class_  
This class exposes the methods: _distance_, _+_ and _/_  
It is mandatory for developers to provide an implementation of each of this methods  
For a more detailed explanation of each method and the conditions they impose,
please refer to the [api](http://balmungsan.gitlab.io/spark-kmeans/#co.edu.eafit.sembigdata.spark.kmeans.PointFunctions)

```scala
//Create a PointFunctions Class for xyPoint
class xyPointFunctions(override val point: xyPoint) extends PointFunctions[xyPoint](point) {
  //euclidean distance
  override def distance(that: xyPoint): Double = {
    val (x1, y1) = this.point.values
    val (x2, y2) = that.values
    import scala.math.{pow, sqrt}
    sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2))
  }
  
  override def +(that: xyPoint): xyPoint = {
    val (x1, y1) = this.point.values
    val (x2, y2) = that.values
    new xyPoint(((x1 + x2), (y1 + y2)))
  }	

  override def /(n: Long): xyPoint = {
    val (x, y) = this.point.values
    new xyPoint((x/n, y/n))
  }
}
```

**Cast Funtion:**  
Also you new to provide a cast function that takes a point an returns a point functions wrapper

```scala
val castFun = (point: xyPoint)=> new xyPointFunctions(point)
```

### Clustering
Now your ready to cluster your data using the KMeans algorithm  

```scala
//transform your data to an RDD of points
val points = data map { case(x, y) => new xyPoint((x,y)) }

//choose a convergence strategy
val cs = ConvergedAfterMeansAreStill(0.1)

//choose an appropriate value for k
val k = 5

//Initialize the kmeans algorithm
val km = new KMeans(points, castFun, cs)

//run (this may take a while)
val clustered = km.kmeans(k)
```

## License
Copyright 2017 Semillero de Investigación en Big Data - Universidad EAFIT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
